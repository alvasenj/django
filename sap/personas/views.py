from django.forms import modelform_factory
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from personas.forms import PersonaForm, DomicilioForm
from personas.models import Persona, Domicilio


def detalle_personas(request, id):
    persona = get_object_or_404(Persona, pk=id)
    return render(request, 'personas/detalle.html', {'persona': persona})


# PersonaForm = modelform_factory(Persona, exclude=[])

def nueva_persona(request):
    if request.method == 'POST':
        forma_persona = PersonaForm(request.POST)
        # Validacion del formulario
        if forma_persona.is_valid():
            forma_persona.save()
            return redirect('index')
    else:
        forma_persona = PersonaForm()
    return render(request, 'personas/nuevo.html', {'forma_persona': forma_persona})


def editar_persona(request, id):
    persona = get_object_or_404(Persona, pk=id)
    if request.method == 'POST':
        forma_persona = PersonaForm(request.POST, instance=persona)
        # Validacion del formulario
        if forma_persona.is_valid():
            forma_persona.save()
            return redirect('index')
    else:
        forma_persona = PersonaForm(instance=persona)
    return render(request, 'personas/editar.html', {'forma_persona': forma_persona})


def eliminar_persona(request, id):
    persona = get_object_or_404(Persona, pk=id)
    if persona:
        persona.delete()
    return redirect('index')


def domicilios(request):
    no_domicilios = Domicilio.objects.count()
    domicilios = Domicilio.objects.order_by('id')
    return render(request, 'domicilios/domicilios.html', {'no_domicilios': no_domicilios, 'domicilios': domicilios})


def detalle_domicilio(request, id):
    domicilio = get_object_or_404(Domicilio, pk=id)
    return render(request, 'domicilios/detalle.html', {'domicilio': domicilio})


def nuevo_domicilio(request):
    if request.method == 'POST':
        forma_domicilio = DomicilioForm(request.POST)
        # Validacion del formulario
        if forma_domicilio.is_valid():
            forma_domicilio.save()
            return redirect('domicilios')
    else:
        forma_domicilio = DomicilioForm()
    return render(request, 'domicilios/nuevo.html', {'forma_domicilio': forma_domicilio})


def editar_domicilio(request, id):
    domicilio = get_object_or_404(Domicilio, pk=id)
    if request.method == 'POST':
        forma_domicilio = DomicilioForm(request.POST, instance=domicilio)
        # Validacion del formulario
        if forma_domicilio.is_valid():
            forma_domicilio.save()
            return redirect('domicilios')
    else:
        forma_domicilio = DomicilioForm(instance=domicilio)
    return render(request, 'domicilios/editar.html', {'forma_domicilio': forma_domicilio})


def eliminar_domicilio(request, id):
    domicilio = get_object_or_404(Domicilio, pk=id)
    if domicilio:
        domicilio.delete()
    return redirect('domicilios')
